package kz.aitu.oop.practice.practice4;

import java.io.File;

public class Accessory {
    private int cost;
    private String name;
    public static File data = new File("src/kz/aitu/oop/practice/practice4/accessories.txt");

    public Accessory(){
    }
    public Accessory(int cost, String name){
        this.cost = cost;
        this.name = name;
    }

    public int getCost(){
        return cost;
    }
    public String getName(){
        return name;
    }

    public void setCost(){
        this.cost = cost;
    }
    public void setName(){
        this.name = name;
    }
}
