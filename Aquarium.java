package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;

public class Aquarium {
    private ArrayList<Inhabitant> inhabitants;
    private ArrayList<Accessory> accessories;

    public Aquarium(){
        inhabitants = new ArrayList<Inhabitant>();
        accessories = new ArrayList<Accessory>();
    }
    public Aquarium(ArrayList<Inhabitant> inhabitants, ArrayList<Accessory> accessories){
        this.inhabitants = inhabitants;
        this.accessories = accessories;
    }

    public ArrayList<Inhabitant> getInhabitants() { return inhabitants; }
    public void setInhabitants(ArrayList<Inhabitant> inhabitants) {
        this.inhabitants = inhabitants;
    }

    public ArrayList<Accessory> getAccessories() {return accessories;}
    public void setAccessories(ArrayList<Accessory> accessories) {this.accessories = accessories;}

    public void addInhabitant(Inhabitant inhabitant){
        inhabitants.add(inhabitant);
    }
    public void addAccessory(Accessory accessory) { accessories.add(accessory);}

    public int getCost(){
        int cost = 0;

        for (Inhabitant inhabitant : inhabitants) {
            cost += inhabitant.getCost();
        }
        for (Accessory accessory : accessories) {
            cost += accessory.getCost();
        }
        return cost;
    }


}
