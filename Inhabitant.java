package kz.aitu.oop.practice.practice4;

public abstract class Inhabitant {
    public abstract int getCost();
    public abstract String getName();

    public abstract void setCost(int cost);
    public abstract void setName(String Name);
}
