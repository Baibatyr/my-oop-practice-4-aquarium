package kz.aitu.oop.practice.practice4;

public class Pencilfish extends  Fish {
    private int cost;
    private String name;
    public Pencilfish(int cost, String name){
        this.cost = cost;
        this.name = name;
    }
    @Override
    public int getCost() {
        return cost;
    }
    public String getName(){
        return name;
    }

    public void setCost(int cost){
        this.cost = cost;
    }
    public void setName(String name){
        this.name = name;
    }
}
