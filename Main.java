package kz.aitu.oop.practice.practice4;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        Aquarium aquarium = new Aquarium();
        boolean bool = true;
        while (bool) {
            System.out.println("Enter '1' to add an Inhabitant.\nEnter '2' to add an accessory.\nEnter '0' to confirm purchase.");
            int t = in.nextInt();
            switch (t){
                case 1:
                    Inhabitant inhabitant;
                    System.out.println("Choose one of the types of inhabitants below:\n\t1)Angelfish\n\t2)Catfish\n\t3)Pencilfish" +
                            "\n\t4)Amazon Tree Boa\n\t5)Caiman Lizard\n\t6)Loggerhead Sea Turtle\nEnter inhabitant number to choose:");
                    int a = in.nextInt();
                    System.out.println("Enter name and cost, for examle: \"Angel 3000\"");
                    String name = in.next();
                    int cost = in.nextInt();
                    switch (a){
                        case 1: inhabitant = new Angelfish(cost, name); aquarium.addInhabitant(inhabitant);break;
                        case 2: inhabitant = new Catfish(cost, name); aquarium.addInhabitant(inhabitant);break;
                        case 3: inhabitant = new Pencilfish(cost, name); aquarium.addInhabitant(inhabitant);break;
                        case 4: inhabitant = new AmazonTreeBoa(cost,name); aquarium.addInhabitant(inhabitant);break;
                        case 5: inhabitant = new CaimanLizard(cost, name); aquarium.addInhabitant(inhabitant);break;
                        case 6: inhabitant = new LoggerheadSeaTurtle(cost, name); aquarium.addInhabitant(inhabitant);break;
                    }
                    System.out.println("Current cost: " + aquarium.getCost());
                    break;
                case 2:
                    Accessory accessory;
                    System.out.println("Choose one of accessories below:");
                    ArrayList<Accessory> accessories = new ArrayList<>();
                    try {
                        Scanner sc = new Scanner(Accessory.data);
                        int i=1;
                        while (sc.hasNext()){
                            String[] s = sc.nextLine().split("---");
                            int costA = Integer.parseInt(s[0]);
                            String nameA = s[1];
                            accessories.add(new Accessory(costA, nameA));
                            System.out.println("\t" + i + ") " + nameA + " - " + costA + "tg");
                            i++;
                        }
                    }catch (FileNotFoundException ex){
                        ex.printStackTrace();
                    }

                    System.out.println("Enter number to choose:");
                    a = in.nextInt();
                    accessory = accessories.get(a-1);
                    aquarium.addAccessory(accessory);
                    System.out.println("Current cost: " + aquarium.getCost());
                    break;
                case 0:
                    bool = false;
                    break;
            }

        }
        System.out.println("Aquarium cost: " + aquarium.getCost());
    }
}
